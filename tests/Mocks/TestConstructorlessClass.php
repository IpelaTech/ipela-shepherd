<?php 
namespace IpelaShepherd\Tests\Mocks;

class TestConstructorlessClass
{
    protected string $name;
    protected bool $is_nice;

    public function get_name() : string
    {
        return $this->name;
    }

    public function get_is_nice() : bool
    {
        return $this->is_nice;
    }
}