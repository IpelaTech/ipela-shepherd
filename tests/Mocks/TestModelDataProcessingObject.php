<?php
namespace IpelaShepherd\Tests\Mocks;

use IpelaShepherd\Contracts\IShepherdDataProcessingObject;

class TestModelDataProcessingObject extends IShepherdDataProcessingObject
{
    public string $name;
    public string $surname;
}