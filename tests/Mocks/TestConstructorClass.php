<?php 
namespace IpelaShepherd\Tests\Mocks;

class TestConstructorClass
{
    protected string $name;
    protected bool $is_nice;

    public function __construct(string $name, bool $is_nice)
    {
        $this->name = $name;
        $this->is_nice = $is_nice;
    }

    public function get_name() : string
    {
        return $this->name;
    }

    public function get_is_nice() : bool
    {
        return $this->is_nice;
    }
}