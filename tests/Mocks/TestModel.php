<?php
namespace IpelaShepherd\Tests\Mocks;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    protected $guarded = [];
}