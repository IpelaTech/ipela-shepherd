<?php 
namespace IpelaShepherd\Tests\Mocks;

use IpelaShepherd\Contracts\IShepherdDataProcessingObject;

class TestDataProcessingObject extends IShepherdDataProcessingObject
{
    protected string $phrase;
}