<?php
namespace IpelaShepherd\Tests\Mocks;

use DateTime;
use IpelaShepherd\Tests\Mocks\TestModel;
use IpelaShepherd\Tests\Mocks\TestDataObject;
use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Tests\Mocks\TestConstructorlessClass;
use IpelaShepherd\Tests\Mocks\TestDataProcessingObject;

class TestDataObject extends IShepherdDataObject
{
    protected int $number;
    protected string $word;
    protected TestDataObject $object;
    protected TestDataProcessingObject $processing_object;
    protected TestModel $model;
    protected TestConstructorClass $tricky_class;
    protected TestConstructorlessClass $easy_class;
    protected DateTime $date;
    protected array $list;
}