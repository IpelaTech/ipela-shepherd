<?php 

use PHPUnit\Framework\TestCase;
use IpelaShepherd\Tests\Mocks\TestDataObject;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

class IShepherdDataObjectTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();
    }

    public function test_can_change_class_member_syntactic_sugar()
    {
        $test_object = ShepherdDataObjectInitialiser::initialise(
            new TestDataObject, [
                "number" => 10
            ]
        );

        $test_object = $test_object->set("number", 20);
        $this->assertEquals(20, $test_object->number);
    }
}