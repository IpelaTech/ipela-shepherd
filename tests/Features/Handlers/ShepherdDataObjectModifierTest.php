<?php

use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;
use IpelaShepherd\Handlers\ShepherdDataObjectModifier;
use PHPUnit\Framework\TestCase;

class ShepherdDataObjectModifierTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();

    }

    public function test_can_modify()
    {
        $step = new TestStep;
        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "word" => "Test"
            ]
        );
        
        $modified_step = ShepherdDataObjectModifier::modify(
            $initialised_step, [
                "word" => "Changed"
            ]
        );

        $this->assertEquals("Changed", $modified_step->word);
    }
}

class TestStep extends IShepherdDataObject
{
    use TestTrait, TestTrait2;
}

trait TestTrait
{
    protected int $number;
    protected string $word;
}

trait TestTrait2
{
    protected TestStep $object1;
}