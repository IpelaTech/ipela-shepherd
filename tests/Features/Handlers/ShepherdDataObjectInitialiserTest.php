<?php 

use PHPUnit\Framework\TestCase;
use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Contracts\IShepherdHandlerInitialiser;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

class ShepherdDataObjectInitialiserTest extends TestCase
{

    public function setUp() : void
    {
        parent::setUp();
    }

    public function test_can_initialise()
    {
        $step = new TestStep;
        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "number" => 5,
                "word" => "Test"
            ]
        );

        $this->assertEquals(5, $initialised_step->number);
        $this->assertEquals("Test", $initialised_step->word);

        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "object1" => $initialised_step
            ]
        );

        $this->assertEquals(5, $initialised_step->object1->number);
        $this->assertEquals("Test", $initialised_step->object1->word);
    }

    public function test_can_initialise_with_data_object_as_instance()
    {
        $step = new TestStep;
        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "number" => 5,
                "word" => "Test"
            ]
        );

        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "object1" => $initialised_step
            ]
        );

        $this->assertEquals(5, $initialised_step->object1->number);
        $this->assertEquals("Test", $initialised_step->object1->word);
    }
    
    public function test_can_initialise_with_array_for_data_object()
    {
        $step = new TestStep;
        $initialised_step = ShepherdDataObjectInitialiser::initialise(
            $step, [
                "object1" => [
                    "number" => 5,
                    "word" => "Test"
                ]
            ]
        );
        
        $this->assertEquals(5, $initialised_step->object1->number);
        $this->assertEquals("Test", $initialised_step->object1->word);
    }

    public function test_can_overload_initialise()
    {
        //using class members to verify that the method worked
        //I guess I could assert no error thrown but it doesn't feel
        //"solid" enough.
        $initialiser = TestInitialiser::initialise("Test");
        $this->assertEquals("Test", $initialiser->name);
        
        $initialiser = TestInitialiser2::initialise("Testing", 55);
        $this->assertEquals("Testing", $initialiser->name);
        $this->assertEquals(55, $initialiser->number);
    }
}
class TestInitialiser extends IShepherdHandlerInitialiser
{
    public string $name;
    public int $number;

    public static function initialise(string $name)
    {
        $self = new static;
        $self->name = $name;
        return $self;
    }
}

class TestInitialiser2 extends IShepherdHandlerInitialiser
{
    public static function initialise(string $name, int $number)
    {
        $self =new static;
        $self->name = $name;
        $self->number= $number;
        return $self;
    }
}

class TestStep extends IShepherdDataObject
{
    use TestTrait, TestTrait2;
}

trait TestTrait
{
    protected int $number;
    protected string $word;
}

trait TestTrait2
{
    protected TestStep $object1;
}