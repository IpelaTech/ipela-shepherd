<?php 
namespace IpelaShepherd\Handlers;

use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Contracts\IShepherdHandlerModifier;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

class ShepherdDataObjectModifier extends IShepherdHandlerModifier
{
    public static function modify(IShepherdDataObject $object_to_modify, array $parameters) : IShepherdDataObject
    {
        $array = $object_to_modify->to_array();
        $modified_parameters = \array_replace($array, (array)$parameters);
        return ShepherdDataObjectInitialiser::initialise($object_to_modify, $modified_parameters);
    }
}