<?php
namespace IpelaShepherd\Handlers;

use DateTime;
use ReflectionClass;
use ReflectionProperty;
use Illuminate\Support\Carbon;
use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Contracts\IShepherdHandlerInitialiser;

class ShepherdDataObjectInitialiser extends IShepherdHandlerInitialiser
{
    public static function initialise(IShepherdDataObject $object_to_initialise, array $parameters = []) : IShepherdDataObject
    {
        return parent::populate_class(\get_class($object_to_initialise), $parameters);
    }
}