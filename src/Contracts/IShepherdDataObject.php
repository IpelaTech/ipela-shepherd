<?php 
namespace IpelaShepherd\Contracts;

use ReflectionClass;
use IpelaShepherd\Contracts\IShepherdDataObject;
use IpelaShepherd\Handlers\ShepherdDataObjectModifier;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

abstract class IShepherdDataObject
{
    public function __get($name)
    {
        if (!\property_exists($this, $name)) {
            throw new \Exception(__CLASS__." property {$name} doesn't exist.");
        }

        return $this->$name;
    }

    public function has(string $trait_name) : bool
    {
        return in_array(
            $trait_name, 
            array_keys((new \ReflectionClass($this))->getTraits())
        );
    }

    public function to_array() : array
    {
        return \get_object_vars($this);
    }

    public function __toString()
    {
        return \json_encode($this->to_array());
    }

    public static function from_array(array $parameters) : IShepherdDataObject
    {
        $self = new static;
        return ShepherdDataObjectInitialiser::initialise($self, $parameters);
    }

    public function set(string $name, $value) : IShepherdDataObject
    {
        return ShepherdDataObjectModifier::modify(
            $this, [
                $name => $value
            ]
        );
    }
}