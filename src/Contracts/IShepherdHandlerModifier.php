<?php 
namespace IpelaShepherd\Contracts;

use IpelaShepherd\Contracts\IShepherdDataObject;

abstract class IShepherdHandlerModifier
{
    public static function __callStatic($name, $arguments)
    {
        if ($name === "modify") { 
            return \call_user_func_array([new static, "modify"], $arguments);
        }
    }
}