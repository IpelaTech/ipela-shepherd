<?php 
namespace IpelaShepherd\Contracts;

use ReflectionClass;
use ReflectionProperty;
use IpelaShepherd\Contracts\IShepherdDataObject;

abstract class IShepherdHandlerInitialiser
{
    public static function __callStatic($name, $arguments)
    {
        if ($name === "initialise") { 
            return \call_user_func_array([new static, "initialise"], $arguments);
        }
    }

    protected static function populate_class(string $class_name, array $parameters) : IShepherdDataObject
    {
        $class = new $class_name;        
        $reflection_class = new ReflectionClass($class);
        
        $properties = $reflection_class->getProperties(
            ReflectionProperty::IS_PRIVATE | 
            ReflectionProperty::IS_PROTECTED | 
            ReflectionProperty::IS_PUBLIC
        );
        
        foreach ($properties as $property) {
            $property_name = $property->name;

            if (null === $property->getType()) { 
                continue;
            }

            $property_type_name = $property->getType()->getName();

            if (strcmp($property_type_name, IShepherdDataObject::class) === 0) {
                throw new \Exception("Cannot instantiate IShepherdDataObject directly. Please change your variable's data type");
            }

            if (\array_key_exists($property_name, $parameters)) {
                $property_value = $parameters[$property_name];

                if ($property_type_name === "DateTime") {
                    //todo write test for date time
                    $date = $property_value;
    
                    /*if (\is_string($property_value)) {
                        $date = Carbon::parse($property_value);
                    }
    
                    if (\is_subclass_of($property_value, DateTime::class)) {
                        $date = Carbon::instance($property_value);
                    }
    
                    if (\is_subclass_of($property_value, Carbon::class)) {
                        $date = $property_value->toDateTime();
                    }*/
                    
                    $property->setAccessible(true);
                    $property->setValue(
                        $class, 
                        $date
                    );
                    $property->setAccessible(false);

                    continue;
                }
                
                if (\is_subclass_of($property_type_name, IShepherdDataObject::class)  
                    && is_array($property_value)
                ) {    
                    $new_class = self::populate_class($property_type_name, $property_value);
                    $property->setAccessible(true);
                    $property->setValue($class, $new_class);
                    $property->setAccessible(false);
                    continue;
                }

                $property->setAccessible(true);
                $property->setValue($class, $property_value);
                $property->setAccessible(false);
            }
        }
        return $class;
    }
}