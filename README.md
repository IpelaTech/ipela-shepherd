# IpelaShepherd 

A fresh take on DTOs.

## Features

Ability to create and modify immutable DTOs.

## Installation

Install with composer

```bash
  composer require ipelatech/ipela-shepherd
```
## Concepts

#### IShepherdDataObject

An abstract class you sublcass to create your "DTO". Only use *protected typed* members in here.

-   Methods
    - public function has(string $trait_name): bool<br>
    Check if there is a trait (use fully qualified name, i.e. with namespace)
    - public function to_array() : array <br>
    Convert IShepherdDataObject to array
    - public static function from_array(array $parameters) : IShepherdDataObject<br>
    Create IShepherdDataObject from array
    - public function set(string $name, $value) : IShepherdDataObject<br>
    Modify variable *$name* in the ISheperherdDataObject
    - public function __toString()
    Prints string of IShepherdDataObject, usage: $object->toString()
    - public function __get($name)
    Allows you to get the value of on of the protected variables in the class
    
#### IShepherdHandlerInitialiser
-   Methods
    - protected function populate_class(string $class_name, array $parameters)
    - public function initialise() : ShepherdDataObject<br>
    You need to implement this if you subclass the Initialiser

#### IShepherdHandlerModifier
-   Methods
     - public function modify() : ShepherdDataObject<br>
    You need to implement this if you subclass the Initialiser

#### ShepherdDataObjectInitialiser
-   Methods:
    -   public static function initialise(IShepherdDataObject $object_to_initialise, array $parameters = []) : IShepherdDataObject

#### ShepherdDataObjectModifier
-   Methods:
    -   public static function modify(IShepherdDataObject $object_to_modify, array $parameters) : IShepherdDataObject

## Usage/Examples

The generalised workflow for working with ShepherdDataObjects is:

1. Create a DTO and extend it with the *IShepherdDataObject* abstract class.
2. Use the ShepherdDataObjectInitialiser or create an initialiser class that extends *IShepherHandlerInitialiser*
3. Instanstiate and use the ShepherdDataObject
4. Use the ShepherdDataObjectModifier or create a class that extends *IShepherdHandlerModifier* to modify values, or use the *set()* convenience method

### Example 

#### Creation

Create DTO with *protected typed* members.

```php
use IpelaShepherd\Contracts\IShepherdDataObject;
use DateTime;

class InvoiceDTO extends IShepherdDataObject {
    protected DateTime $date;
    protected string $client;
    protected int $amount;
    protected string $type;
}

```

You can instantiate a ShepherdDataObject by:

- using the ShepherdDataObjectInitialiser

```php
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

$invoice = ShepherdDataObjectInitialiser::initialise(
    new InvoiceDTO,
    [
        "client" => $client,
        "amount" => $amount,
        "date" => new DateTime(),
        "type" => "full"
    ]
);
```
- or subclassing IShepherdHandlerInitialiser

```php
use IpelaShepherd\Contracts\IShepherdHandlerInitialiser;
use IpelaShepherd\Handlers\ShepherdDataObjectInitialiser;

class FullInvoiceInitialiser extends IShepherdHandlerInitialiser {
    public static function initialise(string $client, int $amount) {
        return ShepherdDataObjectInitialiser::initialise(
            new InvoiceDTO,
            [
                "client" => $client,
                "amount" => $amount,
                "date" => new DateTime(),
                "type" => "full"
            ]
        );
    }
}
```
Now every time you need to create an invoice and you can get all properties (because of the _get() magic method in IShepherdDataObject):

```php
$invoice = FullInvoiceInitialiser::initialise("Client", 1500);

echo $invoice->client; //prints Client
echo $invoice->amount; //prints 1500
echo $invoice->date; //prints current date time
echo $invoice->type; //prints full
```

The power of this method is you can do things like this:

```php 
class ProrataInvoiceInitialiser extends IShepherdHandlerInitialiser {
    public static function initialise(string $client, int $daily_rate) {

        $today = new DateTime();
        $today_day = (int)$today->format("d");
        $days_in_month = date("t");
        $diff = $days_in_month - $today_day;


        return ShepherdDataObjectInitialiser::initialise(
            new InvoiceDTO,
            [
                "client" => $client,
                "amount" => $daily_rate * $diff,
                "date" => new DateTime(),
                "type" => "pro-rata"
            ]
        );
    }
}
```

Every Prorata Invoice will have it's amount set by default with you only passing in the rate and client.

#### Modification

You can modify values by:

- using set
```php
$invoice = $invoice->set("client", "Another Client");
echo $invoice->client;//prints Another Client
```

- or using the ShepherdDataObjectModifier
```php
$invoice = ShepherdDataObjectModifier::modify(
    $invoice,
    ["amount" => 2000]
);

echo $invoice->amount; //prints 2000
```

- or subclassing IShepherdHandlerModifier
```php
use IpelaShepherd\Contracts\IShepherdHandlerModifier;
use IpelaShepherd\Handlers\ShepherdDataObjectModifier;

class FullInvoiceModifier extends IShepherdHandlerModifier {
    public static function modify(InvoiceDTO $invoice, string $client) {
        return ShepherdDataObjectModifier::modify(
            $invoice,
            [
                "client" => $client,
            ]
        );
    }
}

$invoice = FullInvoiceModifier::modify($invoice, "New Client");

echo $invoice->client;//prints New Client
```
### Roadmap 

Shepherd is feature-complete.

## License

[MIT](https://choosealicense.com/licenses/mit/)